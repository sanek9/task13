#include <windows.h> // заголовочный файл, содержащий WINAPI
#include <iostream>
#include <fstream>
#include <string>
#include <locale>

#define ID_BUTTON_1 3000
#define ID_EDIT_1 3001
#define ID_LISTBOX_1 3002
#define ID_CHECKBOX_1 3003
using namespace std;
// Прототип функции обработки сообщений с пользовательским названием:
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
ATOM createMainWindowClass(HINSTANCE, WNDPROC, LPCTSTR);
HWND hMainWnd;
HINSTANCE hInst;

bool check = false;
// Управляющая функция:
int WINAPI WinMain(HINSTANCE hInst, // дескриптор экземпляра приложения
                   HINSTANCE hPrevInst, // не используем
                   LPSTR lpCmdLine, // не используем
                   int nCmdShow) // режим отображения окошка
{
	::hInst = hInst;
	TCHAR szClassName[] = "WinClass"; // строка с именем класса
   // HWND hMainWnd; // создаём дескриптор будущего окошка
   // WNDCLASSEX wc =

    if(!createMainWindowClass(hInst, WndProc, szClassName))
		return -1;
    // Функция, создающая окошко:
    hMainWnd = CreateWindow(
        szClassName, // имя класса
        "Program (V1)",//"Полноценная оконная процедура", // имя окошка (то что сверху)
		WS_OVERLAPPEDWINDOW,
	//	WS_BORDER|WS_CAPTION, // режимы отображения окошка
        CW_USEDEFAULT, // позиция окошка по оси х
        CW_USEDEFAULT, // позиция окошка по оси у (раз дефолт в х, то писать не нужно)
        250,//sx, // ширина окошка
        200, // высота окошка (раз дефолт в ширине, то писать не нужно)
        (HWND)NULL, // дескриптор родительского окна
        NULL, // дескриптор меню
        HINSTANCE(hInst), // дескриптор экземпляра приложения
        NULL); // ничего не передаём из WndProc
    if(!hMainWnd){
        // в случае некорректного создания окошка (неверные параметры и тп):
        MessageBox(NULL, "Не получилось создать окно!", "Ошибка", MB_OK);
        return NULL;
    }

  //  SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)&KeyboardProc, GetModuleHandle(NULL), 0);
     // отображаем окошко

	ShowWindow(hMainWnd, nCmdShow);
//	 ShowWindow(hBtn1, SW_SHOW);
	// ShowWindow(hEdit1, SW_SHOW);
    UpdateWindow(hMainWnd); // обновляем окошко
	MSG msg;
    while(GetMessage(&msg, NULL, NULL, NULL)){ // извлекаем сообщения из очереди, посылаемые фу-циями, ОС
        TranslateMessage(&msg); // интерпретируем сообщения
        DispatchMessage(&msg); // передаём сообщения обратно ОС
    }
    return msg.wParam; // возвращаем код выхода из приложения
}
ATOM createMainWindowClass(HINSTANCE hInst, WNDPROC WndProc, LPCTSTR szClassName){
	WNDCLASSEX wc; // создаём экземпляр, для обращения к членам класса WNDCLASSEX
    wc.cbSize        = sizeof(wc); // размер структуры (в байтах)
    wc.style         = CS_HREDRAW | CS_VREDRAW; // стиль класса окошка
    wc.lpfnWndProc   = WndProc; // указатель на пользовательскую функцию
    wc.lpszMenuName  = NULL; // указатель на имя меню (у нас его нет)
    wc.lpszClassName = szClassName; // указатель на имя класса
    wc.cbWndExtra    = NULL; // число освобождаемых байтов в конце структуры
    wc.cbClsExtra    = NULL; // число освобождаемых байтов при создании экземпляра приложения
    wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO); // декриптор пиктограммы
    wc.hIconSm       = LoadIcon(NULL, IDI_WINLOGO); // дескриптор маленькой пиктограммы (в трэе)
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW); // дескриптор курсора
   // wc.hbrBackground = CreateSolidBrush(RGB(255, 0, 0));//(HBRUSH)(COLOR_WINDOW+1);
    wc.hbrBackground = CreateSolidBrush ( RGB ( 220, 220, 220));//(HBRUSH)GetStockObject(WHITE_BRUSH); // дескриптор кисти для закраски фона окна
    wc.hInstance     = hInst; // указатель на строку, содержащую имя меню, применяемого для класса
	return RegisterClassEx(&wc);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){
	static HWND listbox, checkbox, hEdit1, hBtn1;
	ofstream F;
    switch(uMsg){
    case WM_CREATE:
		listbox = CreateWindow("listbox","bla",WS_CHILD|WS_VISIBLE|LBS_NOTIFY|WS_VSCROLL|LBS_DISABLENOSCROLL|WS_BORDER,20 , 20, 200,100,
			hWnd, (HMENU)ID_LISTBOX_1, hInst, NULL);
		checkbox = CreateWindow("button","UPPER",WS_CHILD|WS_VISIBLE|WS_BORDER|BS_AUTOCHECKBOX,20 , 120, 200,20,
			hWnd, (HMENU)ID_CHECKBOX_1, hInst, NULL);
		hEdit1 = CreateWindow("EDIT","",BS_TEXT|WS_CHILD|WS_VISIBLE|WS_BORDER,20 , 143, 140, 20,
			hWnd, (HMENU)ID_EDIT_1, hInst, NULL);
		hBtn1 = CreateWindow("BUTTON","Add",BS_PUSHBUTTON|WS_CHILD|WS_TABSTOP|WS_VISIBLE|WS_BORDER, 162, 143, 58,20,
			hWnd, (HMENU)ID_BUTTON_1, hInst, NULL);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam)) {
			case ID_BUTTON_1:
				if(HIWORD(wParam)==0){
					int l = GetWindowTextLength(hEdit1)+1;
					TCHAR buf[l];
					GetWindowText( hEdit1, buf, l );

					if(check){
						locale loc;
						for(int i = 0; i < strlen(buf); i++)
							buf[i] = toupper(buf[i], loc);
					}
					SendMessage(listbox,LB_ADDSTRING,0,(LPARAM)buf);
					SetWindowText(hEdit1, "");
				}
//				cout << 1 << endl;
				break;
			case ID_CHECKBOX_1:
				if(HIWORD(wParam)==0){
					WORD res = (WORD)SendMessage(checkbox,BM_GETCHECK,0,0L);
					LONG_PTR p = GetWindowLongPtr(hEdit1, GWL_STYLE);
					if(res == 1){
		//				p = p | ES_UPPERCASE;
						check = true;
					}else if(res == 0){
		//				p = p & ~ES_UPPERCASE;
						check = false;
					}
					SetWindowLong(hEdit1, GWL_STYLE, p);
				}

				break;
	/*		case ID_EDIT_1:
				switch(HIWORD(wParam)){
					case  EN_SETFOCUS:
						cout <<"focus" << endl;
				}
				cout << 3 << endl;
				break;*/
			case ID_LISTBOX_1:
				if(HIWORD(wParam)==LBN_DBLCLK){
					int cur = SendMessage(listbox,LB_GETCURSEL, 0, 0L);
					if(cur>=0)
						SendMessage(listbox, LB_DELETESTRING, cur, 0L);

				}

//				cout << "wq " << HIWORD(wParam) << endl;
				break;
		}

		break;

    case WM_DESTROY: // если окошко закрылось, то:

		F.open("results.txt", ios::out);
		int sz;
		sz = SendMessage(listbox, LB_GETCOUNT, 0, 0L);
		int l;
		for(int i = 0; i < sz; i++){
			l = SendMessage(listbox, LB_GETTEXTLEN, i, 0L);
			TCHAR buf[l+1];
			SendMessage(listbox, LB_GETTEXT, i, (LPARAM)buf);
			F << buf<<endl;
		}
		F.close();
        PostQuitMessage(NULL); // отправляем WinMain() сообщение WM_QUIT
        break;
    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam); // если закрыли окошко
    }
    return NULL; // возвращаем значение
}
